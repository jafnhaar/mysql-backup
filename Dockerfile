FROM alpine:3.17

RUN mkdir /app

COPY ./script.sh /app
COPY ./.common.sh /app
COPY ./docker-entrypoint.sh /

WORKDIR /backup

ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.2.1/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=d7f4c0886eb85249ad05ed592902fa6865bb9d70

RUN apk add --no-cache mysql-client bash mariadb-connector-c aws-cli &&\
    chmod +x /app/script.sh &&\
    chmod +x /docker-entrypoint.sh

RUN wget "$SUPERCRONIC_URL" \
    && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
    && chmod +x "$SUPERCRONIC" \
    && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
    && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic
# RUN ls -la / &&\
#     ls -la /app


ENTRYPOINT [ "/docker-entrypoint.sh" ]
