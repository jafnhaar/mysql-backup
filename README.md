# Basic Information
## 1.1 List of available ENVs
| Variable              | Default Value | Required |
|-----------------------|---------------|----------|
| DATABASE_NAME         |               | required |
| MYSQL_HOST            |               | required |
| MYSQL_USERNAME        |               | required |
| MYSQL_PASSWORD        |               | required |
| BACKUP_QUANTITY       |             10| optional |
| CRONTAB               |     * * * * * | optional |
| RESTORE               |             0 | optional |
| LOG_VERBOSITY         |             8 | optional |
| AWS_ACCESS_KEY_ID     |               | optional |
| AWS_SECRET_ACCESS_KEY |               | optional |
| AWS_DEFAULT_REGION    |               | optional |
| AWS_BUCKET            |               | optional |
| AWS_ENDPOINT_URL      |               | optional |

## 1.2 Required mounts
  - **/backup** - By default this container does backups to **/backup** directory. So this directory should be mounted somewhere.

Docker-compose 
```docker-compose
version: '3'
services:
    mysql-backup:
        image: mysql-backup
        volumes:
            - backup-volume:/backup
volumes:
    backup-volume: {}
```
Docker 
```bash
docker volume create backup-volume
docker run -d -v backup-volume:/backup mysql-backup
```
**This examples still requires all the ENVs **

## 1.3 Backup restoration
If you want to restore your last local backup set this ENVs:
 - **RESTORE**: 1  

If you want to restore your last backup from s3 set this ENVs:
 - **RESTORE**: 2

## 1.4 Basic docker-compose config

```
version: '3'
services:
  mysql-backup:
    build:
      context: .
    restart: unless-stopped
    environment:
      DATABASE_NAME: mydatabase
      MYSQL_HOST: mysql-db
      MYSQL_USERNAME: mysqluser
      MYSQL_PASSWORD: supersecretpassword
      BACKUP_QUANTITY: 30 (will preserve 30 last backups)
      CRONTAB: "0 3 */2 * *" # every second day of month at 03:00
    volumes:
      - /srv/data/mysql-backup:/backup
```
