#!/bin/bash

[ "${DEBUG:-0}" -gt 0 ] && set -exo pipefail || set -eo pipefail

source /app/.common.sh

RESTORE=${RESTORE:-0}

# check that all required ENVs are set correctly
if ! mysql_config_check; then
    echo "Some of the required variables are not set. Exiting now"
    exit 1
fi

#backup restoration
if [ "$RESTORE" -eq 1 ]; then
    #restore from last local backup
    LAST_BACKUP_FILE=$(ls -ltc /backup | grep -i '.sql.gz' | head -n 1| awk '{print $9}' || true)

    if [ -z "$LAST_BACKUP_FILE" ]; then
        echo "No backup found"
        exit 1
    fi
    
    gunzip < $LAST_BACKUP_FILE | mysql -u $MYSQL_USERNAME -p${MYSQL_PASSWORD} $DATABASE_NAME
    echo "Backup restored completed successfully"
    exit 0
fi

if [ "$RESTORE" -eq 2 ] && s3_config_check; then
    #restore from last s3 backup
    LAST_BACKUP_FILE=$(aws s3 --endpoint-url=$AWS_ENDPOINT_URL ls s3://$AWS_BUCKET/$DATABASE_NAME/ | tail -n 1 | awk '{print $4}')
    
    if [ -z "$LAST_BACKUP_FILE" ]; then
        echo "No backup found"
        exit 1
    fi

    aws s3 --endpoint-url=$AWS_ENDPOINT_URL cp s3://$AWS_BUCKET/$DATABASE_NAME/$LAST_BACKUP_FILE /tmp/$LAST_BACKUP_FILE
    gunzip < /tmp/$LAST_BACKUP_FILE | mysql -u $MYSQL_USERNAME -p${MYSQL_PASSWORD} $DATABASE_NAME
    echo "Backup restore completed successfully"
    exit 0
fi

# check if provided creds is right (still requires some work)
# mysql -h $MYSQL_HOST -u $MYSQL_USERNAME -p $MYSQL_PASSWORD -e"quit"

echo "${CRONTAB:-*	*	*	*	*}	/app/script.sh" > /etc/crontabs/root

/usr/local/bin/supercronic -overlapping /etc/crontabs/root
