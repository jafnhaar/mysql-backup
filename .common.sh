#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

# returns true if all mysql connection variables are set properly
mysql_config_check() {
    # exit 1 if variables required for db connection is not set
    [ -z "$DATABASE_NAME" ] && echo "Required variable DATABASE_NAME not set" && return 1
    [ -z "$MYSQL_HOST" ] && echo "Required variable MYSQL_HOST not set" && return 1
    [ -z "$MYSQL_PASSWORD" ] && echo "Required variable MYSQL_PASSWORD not set" && return 1
    [ -z "$MYSQL_USERNAME" ] && echo "Required variable MYSQL_USERNAME not set" && return 1
    return 0
}

# returns true if all s3 required variables are set properly
s3_config_check() {
    [ -z "$AWS_ACCESS_KEY_ID" ] && echo "Required S3 variable AWS_ACCESS_KEY_ID is not set" && return 1
    [ -z "$AWS_SECRET_ACCESS_KEY" ] && echo "Required S3 variable AWS_SECRET_ACCESS_KEY is not set" && return 1
    [ -z "$AWS_DEFAULT_REGION" ] && echo "Required S3 variable AWS_DEFAULT_REGION is not set" && return 1
    [ -z "$AWS_BUCKET" ] && echo "Required S3 variable AWS_BUCKET is not set" && return 1
    [ -z "$AWS_ENDPOINT_URL" ] && echo "Required S3 variable AWS_ENDPOINT_URL is not set" && return 1
    return 0
}

last_backup_file_check() {
    [ -z "$LAST_BACKUP_FILE" ] && echo "Last backup file not found" && exit 1 || return 0
}
