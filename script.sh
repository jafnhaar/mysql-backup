#!/bin/bash
# set ENV var DEBUG to 1 to enable debug mode
[ "${DEBUG:-0}" -gt 0 ] && set -exo pipefail || set -eo pipefail

source /app/.common.sh

BACKUP_QUANTITY=${BACKUP_QUANTITY:-10}
BASEDIR="/backup"
FILE="${BASEDIR}/${DATABASE_NAME}-`date +'%Y%m%d-%H%M'`.sql"

# Backup logic
echo "Starting mysql dump"
/usr/bin/mysqldump --opt --protocol=TCP --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${DATABASE_NAME} > ${FILE}
gzip -8 ${FILE}

# Send current backup to s3
if s3_config_check; then
    echo "Sending current backup to s3"
    aws --endpoint-url=$AWS_ENDPOINT_URL s3 cp ${FILE}.gz s3://$AWS_BUCKET/$DATABASE_NAME/
else
    echo "Skipping s3 backup. S3 required variables are not set"
fi

# backup policy
echo "Deleting backups except ${BACKUP_QUANTITY:-10} most recent one"
# local
ls -t ${BASEDIR} | grep '.sql.gz' | tail -n +$((${BACKUP_QUANTITY}+1)) | xargs rm -f --

# remote
if s3_config_check; then
    aws s3 ls --endpoint=$AWS_ENDPOINT_URL s3://$AWS_BUCKET/$DATABASE_NAME --recursive |\
        awk '{print $4}' | sort -r | grep '.sql.gz' | tail -n +$((${BACKUP_QUANTITY}+1)) |\
        xargs -I FILE aws s3 rm --endpoint=$AWS_ENDPOINT_URL s3://$AWS_BUCKET/FILE
else
    echo "Skipping s3 backup shrink"
fi

echo "Backup script finished"
